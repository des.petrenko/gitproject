<?php
echo '1. Написать программу, которая выводит простые числа, т.е. делящиеся без остатка только на себя и на 1. ';

$lst = array();
$k = 'prime';

for ($i = 2; $i < 100; $i++) {
    for ($j = 2; $j < $i; $j++) {
        if (($i % $j) == 0) {
            $k = 'not prime';
        }
    }

    if ($k == 'prime') {
        $lst[] = $i;
    } else {
        $k = 'prime';
    }
}
foreach ($lst as $list) {
    echo $list . ' ';
}

// Сгенерируйте 100 раз новое число и выведите на экран количество четных чисел из этих 100.
for ($i = 0; $i < 100; $i++) {
    $value = rand(1, 100);
    if ($value % 2 === 0) {
        echo $value;
    }
}
'<br>';
echo '3 Сгенерируйте 100 раз число от 1 до 5 и выведите на экран сколько раз сгенерировались эти числа (1, 2, 3, 4 и 5).';
echo '<br>';
$fuckingArray = [];
for ($i = 0; $i <= 100; $i++) {
    $fuckingIndex = rand(1, 5);
    $fuckingArray[$fuckingIndex] = ($fuckingArray[$fuckingIndex] ?? 0) + 1;
}
print $fuckingArray;

'<br>';
echo ' 4 Используя условия и циклы сделать таблицу в 5 колонок и 3 строки (5x3), отметить разными цветами часть ячеек.';
'<br>';
echo "<table border='1'>";
for ($a = 1; $a <= 3; $a++) {
    if ($a == 2) {
        $k = '<tr style="background-color: blue">';
    }
    echo '<tr>' . $k;
    for ($b = 1; $b <= 5; $b++) {
        echo '<td>';
    }

}

echo '<br>';
echo '<br>';
echo '<br>';

echo 'В переменной month лежит какое-то число из интервала от 1 до 12. 
Определите в какую пору года попадает этот месяц (зима, лето, весна, осень).';
$month = rand(1, 12);
if ($month >= 3 && $month < 6) {
    echo 'Весна';
} elseif ($month >= 6 && $month < 9) {
    echo 'Лето';
} elseif ($month >= 9 && $month < 12) {
    echo 'Осень';
} elseif ($month == 1 || $month == 2 || $month == 12) {
    echo 'Зима';
}
echo '<br/>';
echo 'Дана строка, состоящая из символов, например, "abcde". 
Проверьте, что первым символом этой строки является буква "a". 
Если это так - выведите "да", в противном случае выведите "нет".';
echo '<br/>';
$str = 'abcde';
if ($str[0] === 'a') {
    echo 'да';
} else {
    echo 'нет';
}
echo '<br>';
echo 'Дана строка с цифрами, например, "12345".
 Проверьте, что первым символом этой строки является цифра 1, 2 или 3.
  Если это так - выведите "да", в противном случае выведите "нет".';
echo '<br>';
$str = '12345';
if ($str[0] == 1 || $str[0] == 2 || $str[0] == 3) {
    echo 'Да';
} else {
    echo 'Нет';
}
echo '<br/>';
echo 'Если переменная test равна true, то выведите "Верно",
 иначе выведите "Неверно". Проверьте работу скрипта при test,
  равном true, false. Напишите два варианта скрипта - тернарка и if else.';
echo '<br/>';
$test = true;
if ($test == true) {
    echo 'верно';
} else {
    echo 'неверно';
}
echo '<br/>';
$test = false;
echo ($test == true) ? 'верно' : 'не верно';
echo '<br/>';
echo 'Дано Два массива рус и англ ["пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс"].
 Если переменная lang = ru вывести массив на русском языке, а если en то вывести на английском языке. 
 Сделать через if else и через тернарку.';
$rus = ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс'];
$eng = ['mo', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];
$lang = 'ru';
if ($lang == 'ru') {
    echo $rus;
} else {
    echo 'не тот язык';
}
echo '<br>';
$lang = 'eng';
echo ($lang == 'eng') ? print $eng : 'не тот язык';
echo '<br>';
echo 'В переменной cloсk лежит число от 0 до 59 – это минуты. 
Определите в какую четверть часа попадает это число (в первую, вторую, третью или четвертую). тернарка и if else.';
echo '<br>';
$clock = mt_rand(0, 59);
if ($clock <= 14) {
    echo 'первая четверть';
} elseif ($clock >= 15 && $clock <= 29) {
    echo 'вторая четверть';
} elseif ($clock >= 29 && $clock <= 44) {
    echo 'третья четверть';
} elseif ($clock >= 45 && $clock <= 59) {
    echo 'четвертая четверть';
}
echo '<br>';

$clock = mt_rand(0, 59);
echo ($clock <= 14) ? 'первая четверть ' : '';
echo ($clock >= 15 && $clock <= 29) ? 'вторая четверть ' : '';
echo ($clock >= 29 && $clock <= 44) ? 'третья четверть ' : '';
echo ($clock >= 45 && $clock <= 59) ? 'четвертая четверть ' : '';
echo '<br>';
echo 'Все задания делаем 4-мя способами

- while
- do while
- for
- foreach';

echo '<br>';
echo 'Реализовать свою функцию count()';
echo '<br>';
// foreach
$array = ['Alex', 'Kostya', 'Pavel', 'Oleg'];
$count = 0;
foreach ($array as $val) {
    $count++;
}
echo $count;
echo '<br>';
//for
$array = ['Alex', 'Kostya', 'Pavel', 'Oleg'];
$count = 0;
for (; $array[$count] != null; $count++) {
    $count++;
}
echo $count;
echo '<br>';
//while
$array = ['Alex', 'Kostya', 'Pavel', 'Oleg'];
$count = 0;
while ($array[$count] != null) {
    $count++;
}
echo $count;
echo '<br>';
//do while
$array = ['Alex', 'Kostya', 'Pavel', 'Oleg'];
$count = 0;
do {
    $count++;
} while ($array[$count] != null);
echo $count;
echo '<br>';
echo 'Дан массив [Alex, Vanya, Tanya, Lena, Tolya].
 Развернуть этот массив в обратном направлении.';
echo '<br>';
$arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$res = [];
//for
for ($i = 5 - 1; $i > 0; $i--) {
    $res[] = $arr[$i];
}
print $res;
echo '<br>';
//while
$i = 5;
while ($i > 0) {
    $i--;
    $res[] = $arr[$i];
}
print $res;
echo '<br>';
//do while
$i = 5;
do {
    $i--;
    $res[] = $arr[$i];
} while ($i > 0);
echo $res;
//foreach не работает
echo '<br>';
$res = [];
$arr = ['Alex', 'Kostya', 'Pavel', 'Oleg'];
$i = 4;
foreach ($arr as $item) {
    $res[$i] = $item;
    $i--;
}
print $res;

echo 'Дан массив [44, 12, 11, 7, 1, 99, 43, 5, 69]. Развернуть этот массив в обратном направлении.';
echo 'Дан массив [Alex, Vanya, Tanya, Lena, Tolya].
 Развернуть этот массив в обратном направлении.';
echo '<br>';
echo 'Дан массив [44, 12, 11, 7, 1, 99, 43, 5, 69]. Развернуть этот массив в обратном направлении.';
echo '<br>';
//for
$fuckingArray = ['44', '12', '11', '7', '1', '99', '43', '5', '59'];
$emptyArr = [];
for ($length = 0; $fuckingArray[$length] != null; $length++) {
    for ($i = $length - 1; $fuckingArray[$i] != null; $i--) {
        $emptyArr[] = $fuckingArray[$i];
    }
}
print $emptyArr;
//while
echo '<br>';
while ($length = 0 & $fuckingArray[$length]) {
    $length++;
}
while ($i = $length & $fuckingArray[$i] != null) {
    $i--;
    $emptyArr[] = $fuckingArray[$i];

}
print $emptyArr;
//do while
echo '<br>';
$fuckingArray = ['44', '12', '11', '7', '1', '99', '43', '5', '59'];
$emptyArr = [];
// do while
$i = 9;
do {
    $i--;
    $emptyArr[] = $fuckingArray[$i];
} while ($i >= 0);
print $emptyArr;
echo '<br>';
//foreach

$fuckingArray = ['44', '12', '11', '7', '1', '99', '43', '5', '59'];
$emptyArr = [];
//foreach
$i = 9;
foreach ($fuckingArray as $item) {
    $emptyArr[$i] = $item;
    $i--;
}
print $emptyArr;
echo '<br>';

echo 'Дана строка $str = Hi I am ALex. Развернуть строку в обратном направлении.';
echo '<br>';
//for
$str = 'Hi I am Alex';
for ($i = 12; $i >= 0; $i--) {
    echo $str[$i];
}
//while
echo '<br>';
$i = 12;
while ($i >= 0) {
    $i--;
    echo $str[$i];
}
echo $str[$i];
echo '<br>';
//do while;
$i = 12;
do {
    echo $str[$i];
} while ($i--);
echo '<br>';
//foreach не работает, проверить
$str = ['H', 'i', 'I', 'a', 'm', 'A', 'l', 'e', 'x'];
$emptyArr = [];

foreach ($str as $item) {
    $i = 9;
    $emptyArr[$i] = $item;
    $i--;
}
echo 'сделать ее с с маленьких букв.';
$str = 'Hi I am Alex';
$smallLetter = '';
$count = 0;
for ($count = 0; $str[$count]; $count++) {
    $i = 0;
    while ($i <= $count) {
        $smallLetter .= strtolower($str[$i]);
        $i++;
    }
}
echo '<br>';
echo $smallLetter;
echo '<br>';
for ($count = 0; $str[$count]; $count++) {
    for ($i = 0; $i <= $count; $i++) {
        $smallLetter .= strtolower($str[$i]);
    }
}
echo '<br>';
echo $smallLetter;
echo '<br>';
for ($count = 0; $str[$count]; $count++) {
    $i = 0;
    do {
        $smallLetter .= strtolower($str[$i]);
        $i++;
    } while ($i <= $count);
}
echo $smallLetter;
echo '<br>';
echo 'сделать все буквы большими';
echo '<br>';
$str = 'Hi I am Alex';
$upLetter = '';
$count = 0;

echo '<br>';

for ($count = 0; $str[$count]; $count++) {
    $i = 0;
    while ($i <= $count) {
        $upLetter .= strtoupper($str[$i]);
        $i++;
    }
}
echo $upLetter;
echo '<br>';
for ($count = 0; $str[$count]; $count++) {
    $i = 0;
    do {
        $upLetter .= strtoupper($str[$i]);
        $i++;
    } while ($i <= $count);
}
echo '<br>';
echo $upLetter;
for ($count = 0; $str[$count]; $count++) {
    for ($i = 0; $i <= $count; $i++) {
        $upLetter .= strtoupper($str[$i]);
    }
}
echo '<br>';

echo $upLetter;

echo 'дан массив, сделать все буквы маленькие';
$fuckingArray = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$smallLetter = [];
$count = 0;
for ($count = 0; $fuckingArray[$count]; $count++);
$i = 0;
for ($i = 0; $i <= $count; $i++) {
    $smallLetter[] .= strtolower($fuckingArray[$i]);
}
echo '<pre>';
print $smallLetter;
echo '</pre>';
foreach ($fuckingArray as $item) {
    $smallLetter[$i] = strtolower($item);
    $i++;
}
echo '<pre>';
print $smallLetter;
echo '</pre>';
for ($count = 0; $fuckingArray[$count]; $count++);
$i = 0;
while ($i <= $count) {
    $smallLetter[] .= strtolower($fuckingArray[$i]);
    $i++;
}
echo '<pre>';
print $smallLetter;
echo '</pre>';
for ($count = 0; $fuckingArray[$count]; $count++);
$i = 0;
do {
    $smallLetter[] .= strtolower($fuckingArray[$i]);
    $i++;
} while ($i <= $count);
echo '<pre>';
print $smallLetter;
echo '</pre>';
echo 'дан массив, сделать все буквы большие';
$fuckingArray = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$smallLetter = [];
$count = 0;
for ($count = 0; $fuckingArray[$count]; $count++);
$i = 0;
for ($i = 0; $i <= $count; $i++) {
    $smallLetter[] .= strtoupper($fuckingArray[$i]);
}
echo '<pre>';
print $smallLetter;
echo '</pre>';
foreach ($fuckingArray as $item) {
    $smallLetter[$i] = strtoupper($item);
    $i++;
}
echo '<pre>';
print $smallLetter;
echo '</pre>';
for ($count = 0; $fuckingArray[$count]; $count++);
$i = 0;
while ($i <= $count) {
    $smallLetter[] .= strtoupper($fuckingArray[$i]);
    $i++;
}
echo '<pre>';
print $smallLetter;
echo '</pre>';
for ($count = 0; $fuckingArray[$count]; $count++);
$i = 0;
do {
    $smallLetter[] .= strtoupper($fuckingArray[$i]);
    $i++;
} while ($i <= $count);
echo '<pre>';
print $smallLetter;
echo '</pre>';
$fuckingArray = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$smallLetter = [];
$count = 0;
for ($count = 0; $fuckingArray[$count]; $count++);
$i = 0;
for ($i = 0; $i <= $count; $i++) {
    $smallLetter[] .= strtoupper($fuckingArray[$i]);
}
echo '<pre>';
print $smallLetter;
echo '</pre>';
foreach ($fuckingArray as $item) {
    $smallLetter[$i] = strtoupper($item);
    $i++;
}
echo '<pre>';
print $smallLetter;
echo '</pre>';
for ($count = 0; $fuckingArray[$count]; $count++);
$i = 0;
while ($i <= $count) {
    $smallLetter[] .= strtoupper($fuckingArray[$i]);
    $i++;
}
echo '<pre>';
print $smallLetter;
echo '</pre>';
for ($count = 0; $fuckingArray[$count]; $count++);
$i = 0;
do {
    $smallLetter[] .= strtoupper($fuckingArray[$i]);
    $i++;
} while ($i <= $count);
echo '<pre>';
print $smallLetter;
echo '</pre>';
echo 'Дано число $num = 1234678, развернуть ее в обратном направлении.';
$num = (string)1234678;
$reverse = '';
$count = 0;
for ($count = 0; $num[$count]; $count++);
for ($i = $count - 1; $i >= 0; $i--) {
    $reverse .= $num[$i];
};
echo $reverse;
echo '<br>';
$i = $count - 1;

while ($i >= 0) {
    $i--;
    $reverse .= $num[$i];
}
echo '<br>';
echo $reverse;

do {
    $i--;
    $reverse .= $num[$i];
} while ($i >= 0);
echo '<br>';
echo $reverse;
echo '<br>';
echo 'Дан массив [44, 12, 11, 7, 1, 99, 43, 5, 69], отсортируй его в порядке убывания.';
$fuckingArray = [44, 12, 11, 7, 1, 99, 43, 5, 69];
$count = 0;
foreach ($fuckingArray as $val) {
    $count++;
}
echo $count;

// перебираем массив
$countArr = $count[$fuckingArray] - 1;
for ($j = 0; $j < $countArr; $j++) {
    for ($i = 0; $i < $countArr - $j; $i++) {
        // если текущий элемент больше следующего
        if ($fuckingArray[$i] > $fuckingArray[$i + 1]) {
            // меняем местами элементы
            $tmp_var = $fuckingArray[$i + 1];
            $fuckingArray[$i + 1] = $fuckingArray[$i];
            $fuckingArray[$i] = $tmp_var;
        }
    }
}print $fuckingArray;
