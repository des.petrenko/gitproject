<?php
echo 'first';
// обычная
/**
 * @param float $a
 * @param float $b
 * @param float $c
 * @return float
 */
function findNum($a, $b, $c): float
if ($a > $b && $a > $c) {
    return $a;
} elseif ($b > $a && $b > $c) {
    return $b;
} elseif ($c > $a && $c > $b) {
    return $c;
}
}

//стрелочная функция
$fnMinMax = fn($a, $b, $c) => ($a > $b && $b > $c) ? ('max: ' . $a . ' min: ' . $c) :
    (($a > $c && $c > $b ? ('max: ' . $a . ' min: ' . $b) :
        (($b > $a && $a > $c) ? ('max: ' . $b . ' min: ' . $c) :
            (($b > $c && $c > $a) ? ('max: ' . $b . ' min: ' . $a) :
                (($c > $a && $a > $b) ? ('max: ' . $c . ' min: ' . $a) :
                    ('max: ' . $c . ' min: ' . $a))))));

// function expression
$fnMinMax = function ($a, $b, $c) {
    return ($a > $b && $b > $c) ? ('max: ' . $a . ' min: ' . $c) :
        (($a > $c && $c > $b ? ('max: ' . $a . ' min: ' . $b) :
            (($b > $a && $a > $c) ? ('max: ' . $b . ' min: ' . $c) :
                (($b > $c && $c > $a) ? ('max: ' . $b . ' min: ' . $a) :
                    (($c > $a && $a > $b) ? ('max: ' . $c . ' min: ' . $a) :
                        ('max: ' . $c . ' min: ' . $a))))));
};


echo 'найти площадь';
//обычная
/**
 * @param float $a
 * @param float $b
 * @return float
 */
function square($a, $b): float
{
    return print $a * $b;
}

// // function expression
/**
 * @param float $a
 * @param float $b
 * @return float
 */
function square2($a, $b): float
{
    $c = $a * $b;
    return print $c;
}

////стрелочная функция
$square3 = fn($a, $b) => $a * $b;

echo $square3(2, 5);

echo 'Теорема Пифагора';
//обычная
/**
 * @param float $a
 * @param float $b
 * @return float
 * /* @ author Denys Petrenko < Des . Petrenko @ gmail . com >
 */
function getNumPifagor($a, $b): float
{
    return $c = $a * $a + $b * $b;
}

//// // function expression
/**
 * @param float $a
 * @param float $b
 * @return float
 * /* @ author Denys Petrenko < Des . Petrenko @ gmail . com >
 */

$getNumPifagor2 = function (double $a, double $b): double {
    return $a * $a + $b * $b;
};
//стрелочная функция
/**
 * @param float $a
 * @param float $b
 * @return float|int
 */
/* @ author Denys Petrenko < Des . Petrenko @ gmail . com >
 */

$getNumPifagor3 = fn(double $a, double $b) => $a * $a + $b * $b;

echo 'Найти периметр';
// обычная функция
/**
 * @param float $a
 * @param float $b
 * @return float
 */
function getPerimetrKvadrat($a, $b): float
{
    return $a * $b;

}

//function expression
/**
 * @param float $a
 * @param float $b
 * @return float
 */
/* @ author Denys Petrenko < Des . Petrenko @ gmail . com >
 */

$getPerimetrKvadrat2 = function (double $a, double $b): double {
    return $a * $b;
};

// стрелочная
/**
 * @param float $a
 * @param float $b
 * @return float|int
 */
/* @ author Denys Petrenko < Des . Petrenko @ gmail . com >
 */

$getPerimetrKvadrat3 = fn(double $a, double $b) => $a * $b;

echo 'Найти периметр';
//обынчая
/**
 * @param float $a
 * @return float
 */
function getPerimetr($a): float
{
    return $a * 4;
}

//expression
/**
 * @param float $a
 * @return float
 */
/* @ author Denys Petrenko < Des . Petrenko @ gmail . com >
 */

$getPerimetrKvadrat = function (double $a): double {
    return $a * 4;
};
/**
 * @param float $a
 * @retur/* @ author Denys Petrenko < Des . Petrenko @ gmail . com >
 * n float|int
 */
/* @ author Denys Petrenko < Des . Petrenko @ gmail . com >
 */

$getPerimetrKvadrat3 = fn(double $a) => $a * 4;

echo 'Создать только четные числа до 100';
//обычная
/* @ author Denys Petrenko < Des . Petrenko @ gmail . com >
 */
/**
 * @return array
 */
function getArrEven(): array
{
    $arr = [];
    for ($i = 0; $i < 100; $i++) {
        if ($i % 2 == 0) {
            $arr[] = $i;
        }
    }
    return $arr;
}

// expression
/**
 * @return array
 */
/* @ author Denys Petrenko < Des . Petrenko @ gmail . com >
 */

$getArrEven2 = function (): array {
    $arr = [];
    for ($i = 0; $i <= 100; $i++) {
        if ($i % 2 == 0) {
            $arr[] = $i;
        }
    }
    return $arr;
};
// по-другому никак, вроде, тела ж нет метода
$getArrEven3 = fn() => range(0, 100, 2);

echo 'Создать нечетные числа до 100';
// expression
/**
 * @return array
 */
/* @ author Denys Petrenko < Des . Petrenko @ gmail . com >
 */

$getArrOdd = function (): array {
    $arr = [];
    for ($i = 0; $i <= 100; $i++) {
        if ($i % 2 != 0) {
            $arr[] = $i;
        }
        return $arr;
    }
};

// по-другому никак, вроде, тела ж нет метода
$getArrEven3 = fn() => range(0, 100, 3);


echo 'Найти дискриминант';
/**
 * @param float $b
 * @param float $a
 * @param float $c
 * @return float
 */
/* @ author Denys Petrenko < Des . Petrenko @ gmail . com >
 */

$GetDiscriminant = function (double $b, double $a, double $c): double {
    return $b ** 2 - 4 * $a * $c;
};

//стрелочной не знаю как это провернуть

echo 'Определите, есть ли в массиве повторяющиеся элементы.';

//обынчая

/**
 * @param array $array
 * @return array
 */
function arrayUnique2($array): array
{
    $to_return = array();
    $current_index = 0;
    $k = count($array);
    for ($i = 0; $i < $k; $i++) {
        $current_is_unique = true;

        for ($a = $i + 1; $a < $k; $a++) {
            if ($array[$i] == $array[$a]) {
                $current_is_unique = false;
                break;
            }
        }
        if ($current_is_unique) {
            $to_return[$current_index] = $array[$i];
        }

    }

    return $to_return;
}

// анонимная
/**
 * @return array
 */

$array_unqui3 = function ($array): array {
    $to_return = array();
    $current_index = 0;
    $k = count($array);
    for ($i = 0; $i < $k; $i++) {
        $current_is_unique = true;
        $k = count($array);
        for ($a = $i + 1; $a < $k; $a++) {
            if ($array[$i] == $array[$a]) {
                $current_is_unique = false;
                break;
            }
        }
        if ($current_is_unique) {
            $to_return[$current_index] = $array[$i];
        }

    }

    return $to_return;
};

$array_unqui4 = fn($arr) => array_unique($arr, $flags = SORT_STRING);
