<?php
session_start();

$phone = htmlspecialchars($_REQUEST['phone']);
$name = htmlspecialchars($_REQUEST['name']);
$address = htmlspecialchars($_REQUEST['address']);
$product = htmlspecialchars($_REQUEST['product']);
$checkOut = [$product => 'product', $phone => 'phone', $name => 'name', $address => 'address'];
array_push($_SESSION['orders'], $checkOut);
