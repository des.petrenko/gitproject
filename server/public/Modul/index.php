<?php

echo 'Создайте функцию, которая подсчитывает количество директорий в массиве.';

$dir = ['/home/denis/PhpstormProjects/a-level-nix-2/server/public/Modul/'];
/* @ author Denys Petrenko < Des . Petrenko @ gmail . com >
 */
/**
 * @param array $dir
 * @return integer
 */
function countDir(array $dir): integer
{
//    проверяем дирректория это
    if (is_dir($dir)) {
//        получаем список файлов в дирректории
        $list = scandir($dir);
//        считаем
        $result = count($dir);
    }
    return $result;

}

echo countDir($dir);

echo 'Создайте функцию, которая принимает массив из трех элементов представляющих собой результат запуска слот-машины из казино.
 Проверьте, является ли комбинация элементов удачной (все элементы равны).';
/* @ author Denys Petrenko < Des . Petrenko @ gmail . com >
 */
/**
 * @param array $arr
 * @return string
 */
function cassino(array $arr): string
{
    if ($arr[0] == $arr[1] && $arr[2] == $arr[3] && $arr[0] == $arr[3]) {
        $lucky = 'Lucky';
    }
    return $lucky;
}

$arr = [7, 7, 7];

print cassino($arr);

echo 'Сэму и Фродо надо держаться вместе. Проверьте, нет ли между ними других персонажей';


$ring = ['Sam', 'Frodo', 'Troll', 'Balrog', 'Human'];
/* @ author Denys Petrenko < Des . Petrenko @ gmail . com >
 */
/**
 * @param array $ring
 * @return boolean
 */
function getLocationSamFrodo(array $ring): boolean
{
    $ring = ['Sam', 'Frodo', 'Troll', 'Balrog', 'Human'];
    $freind1 = array_search('Frodo', $ring);
    $freind2 = array_search('Balrog', $ring);
    if (abs($freind1 - $freind2) === 1) {
        return ('Друзья рядом');
    } else {
        return ('Друзья далеко');
    }
}

echo 'Найдите второе наибольшее число в массиве';
/* @ author Denys Petrenko < Des . Petrenko @ gmail . com >
 */
/**
 * @param array $arrNum
 * @return integer
 */
function getSecondBiggerNum(array $arrNum): integer
{
    $res = array_unique($arrNum) && ksort($arrNum);
    return $arrNum[1];
}


getSecondBiggerNum($arrNum);
$arrNum = [1, 2, 3];

echo 'Дан массив строк, создайте функцию, которая создает новый массив, содержащий строки, длины которых соответствуют наидлиннейшей строке';
$arrayString = ['in', 'Soviet', 'Russia', 'frontend', 'programms'];
/* @ author Denys Petrenko < Des . Petrenko @ gmail . com >
 */
/**
 * @param array $arrayString
 * @return array
 */
function getNewArrayFromString(array $arrayString): array
{
    $size = count($arrayString);
    $newArr = [];
    for ($i = 0; $i < $size; $i++) {

        $newArr = substr_count($arrayString[$i]);

        $newArr = ksort($arrayString);

        $NewArr = array_fill($arrayString[0]);
    }
    return $newArr;
}


echo 'Рассчитайте финальную оценку студента по пяти предметам. 
Если средняя оценка больше 90, то итоговая A. 
Если средняя оценка больше 80, то итоговая B. 
Если средняя оценка больше 70, то итоговая оценка C.
 Если средняя оценка больше 60, то итоговая оценка D. 
 В остальных случаях итоговая оценка F.';

$studentGrade = [90, 91, 99, 93, 100];
/* @ author Denys Petrenko < Des . Petrenko @ gmail . com >
 */
/**
 * @param array $studentGade
 * @return string
 */
function getFinalStudentGrade(array $studentGade): string
{
    $size = count($studentGade);
    $sum = 0;
    $arr = 0;
    $count = 0;
    for ($i = 0; $i < $size; $i++) {
        $sum += $arr[$i];
        $count++;
        $totalGrade = $sum / $count;
    }
    if ($totalGrade > 90) {
        return 'A';
    } elseif ($totalGrade > 80 && $totalGrade < 90) {
        return 'b';
    } elseif ($totalGrade > 70 && $totalGrade < 80) {
        return 'C';
    } elseif ($totalGrade > 60 && $totalGrade < 70) {
        return 'D';
    } else {
        return 'F';
    }
}

echo 'Фермер просит вас посчитать сколько ног у всех его животных.
 Фермер разводит три вида: курицы = 2 ноги коровы = 4 ноги свиньи = 4 ноги 
 Фермер посчитал своих животных и говорит вам, сколько их каждого вида. 
 Вы должны написать функцию, которая возвращает общее число ног всех жив';

/* @ author Denys Petrenko < Des . Petrenko @ gmail . com >
 */
/**
 * @param integer $heh
 * @param integer $cow
 * @param integer $pig
 * @return integer
 */
function countLegsAnimal($heh, $cow, $pig): integer
{
    $heh = $heh * 2;
    $cow = $cow * 4;
    $pig = $pig * 4;
    $legs = $heh + $cow + $pig;
    return $legs;
}


echo 'Создайте функцию, которая трансформирует массив слов в массив длин этих слов';
/* @ author Denys Petrenko < Des . Petrenko @ gmail . com >
 */
/**
 * @param array $wordsArr
 * @return array
 */
function getArrayFromWords(array $wordsArr): array
{
    $size = count($wordsArr) - 1;
    $arrString = [];
    for ($i = 0; $i < $size; $i++) {
        $arrString[] .= strlen($wordsArr[$i]);

    }
    return $arrString;
}


$wordsArr = ['some', 'test', 'data', 'strings'];

echo 'Создайте функцию, которая принимает слово на английском языке и проверяет,
 во множественном ли числе находится слово. Проверяйте самый простой вариант.';
$word = 'fork';
/* @ author Denys Petrenko < Des . Petrenko @ gmail . com >
 */
/**
 * @param string $word
 * @return boolean
 */
function getEnglishPluralWords($word): bool
{
    if (substr($word, -1) == 's') {
        return 'множественное число';
    } else {
        return '';

    }
}
