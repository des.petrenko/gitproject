<?php

/**
 * @ author Denys Petrenko < Des . Petrenko @ gmail . com >
 * @param array  $arr
 * @param string $str
 * @return array
 */
function searchArr(array $arr, $str = 'desc'): array
{
    if ($str == 'asc') {
        $k = count($arr);
        for ($i = 0; $i < $k; $i++) {
            for ($j = $i; $j < $k; $j++) {
                if ($arr[$i] > $arr[$j]) {
                    $temp = $arr[$i];
                    $arr[$i] = $arr[$j];
                    $arr[$j] = $temp;
                }
            }
        }
    } elseif ($str == 'desc') {
        for ($i = 0; $i < $k; $i++) {
            for ($j = $i; $j < $k; $j++) {
                if ($arr[$i] < $arr[$j]) {
                    $temp = $arr[$i];
                    $arr[$i] = $arr[$j];
                    $arr[$j] = $temp;
                }
            }
        }
    }
    return $arr;
}

/** @ author Denys Petrenko < Des . Petrenko @ gmail . com >
 * @param array $array
 * @return array
 */
function arrayUnique2(array $array): array
{
    $to_return = array();
    $current_index = 0;
    $k  = count($array);
    for ($i = 0; $i < $k; $i++) {
        $current_is_unique = true;

        for ($a = $i + 1; $a < $k; $a++) {
            if ($array[$i] == $array[$a]) {
                $current_is_unique = false;
                break;
            }
        }
        if ($current_is_unique) {
            $to_return[$current_index] = $array[$i];
        }

    }

    return $to_return;
}

$arr = [1, 1, 5, 5, 7, 7, 9, 9];
