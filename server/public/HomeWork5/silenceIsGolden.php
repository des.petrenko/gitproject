<?php
echo 'Выведите на экран n раз фразу Silence is golden. Число n вводит пользователь на форме. Если n некорректно, вывести фразу Bad n.';
echo '<br>';

if (empty($_POST)) {
    echo 'данные не отправлены';
} else {
    $number = $_POST['num'];
    if (is_numeric($_POST['num'])) {
        $i = 1;
        while ($i <= $number) {
            $i++;
            echo 'silence is golden<br>';
        }
    } else {
        echo 'Bad n . {$number}';
    }
}
