<?php

/** Суммирует числа
 * @param float $a
 * @param float $b
 * @param float $c
 * @return string
 */
/* @ author Ivan < Des . Petrenko @ gmail . com >
 */
function sum(float $a, float $b, float $c): float
{
    if ($a && $b && $c) {
        return $a + $b + $c;
    }
}

sum(3, 5, 9);